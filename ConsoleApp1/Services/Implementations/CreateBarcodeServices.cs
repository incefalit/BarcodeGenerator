﻿using BarcodeGenerator.Application.Services.Interfaces;
using ZXing;
using ZXing.Datamatrix;

namespace BarcodeGenerator.Application.Services.Implementations
{
    public class CreateBarcodeServices : ICreateBarcode
    {
        public string CreateNewBarcode(string str)
        {
            var writer = new BarcodeWriterSvg()
            {
                Format = BarcodeFormat.DATA_MATRIX,
                Options = new DatamatrixEncodingOptions
                {
                    Height = 100,
                    Width = 100,
                }
            };

            File.WriteAllText("Code.svg", writer.Write(str).Content, System.Text.Encoding.UTF8);
            var path = Path.GetFullPath("Code.svg");

            return path;
        }
    }
}
