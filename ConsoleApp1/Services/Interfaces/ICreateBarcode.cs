﻿namespace BarcodeGenerator.Application.Services.Interfaces
{
    public interface ICreateBarcode
    {
        string CreateNewBarcode(string str);
    }
}
