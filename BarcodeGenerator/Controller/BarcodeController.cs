﻿using BarcodeGenerator.Application.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BarcodeGenerator.Controller
{
    public class BarcodeController : ControllerBase
    {
        private readonly ICreateBarcode _createBarcode;
        public BarcodeController(ICreateBarcode createBarcode)
        {
            _createBarcode = createBarcode;
        }

        [HttpGet("Create_Barcode")]
        public async Task<IActionResult> CreateNewBarcode(string code)
        {
            try
            {
                var path = _createBarcode.CreateNewBarcode(code);
                return Ok(path);
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
